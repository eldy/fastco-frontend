1. What were the most difficult tasks?
   It seems straightforward at first, but I nearly ended up using 2 movie states to hold the movies from the api and movies from the saved playlist... eventually, I decided to combine them into a single state, because it's easier to manage.

2. Did you learn anything new while completing this assignment?
   Read a bit more on the react-query's doc, looking at some of the options that I haven't used before.

3. What did you not have time to add? What work took the up majority of your
   time?
   Test? There's no test for this. I spend most writing the code for saving movies to the playlist and how to sync 2 pages together. I've to use react-query sightly differently than my normal use case for this. Also reading chakra-ui's doc for some css adjustment.

4. How could the application be improved?
   Do you mean the functionalities?

   There should be an option to let the user remove a movie from the playlist.

   Routing. After the user made a search, it should have a query like example.com/?query=superman so that the user won't lose the searched result when they navigate around and come back from other routes.

   Pagination. Now we display only the first 10 results.
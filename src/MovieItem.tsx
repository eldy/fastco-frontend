import React from "react";
import { Container, Box, Text, Button } from "@chakra-ui/react";
import type { IMovie } from "./hooks/useMovies";

interface MovieComponentProps {
  movie: IMovie;
  addToPlaylistClick?: (movie: IMovie) => void;
}

const MovieItem: React.FC<MovieComponentProps> = ({
  movie,
  addToPlaylistClick,
}) => (
  <Container
    key={movie.imdbID}
    display="flex"
    flexDirection="column"
    padding="0"
    mb="16px"
    minH="100%"
  >
    <Box
      w="100%"
      h="10"
      bg="#8b5d8f"
      height="200px"
      mb={{
        xs: "10px",
      }}
      bgImage={movie.Poster}
      bgSize="cover"
    ></Box>
    <Box background="lightGrey" p={2} pt="none" fontSize="md">
      {movie.Title} ({movie.Year})<Text fontStyle="italic">{movie.Type}</Text>
      {addToPlaylistClick && (
        <Button
          w="100%"
          mt="10px"
          onClick={() => addToPlaylistClick(movie)}
          disabled={movie.inPlaylist}
        >
          {movie.inPlaylist ? "In playlist" : "Add to playlist"}
        </Button>
      )}
    </Box>
  </Container>
);

export default MovieItem;

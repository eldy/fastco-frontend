import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  Button,
  Text,
} from "@chakra-ui/react";
import type { IMovie } from "./hooks/useMovies";

const MyModal = ({
  onClose,
  isOpen,
  addToPlaylist,
  movie,
}: {
  isOpen: boolean;
  onClose: () => void;
  addToPlaylist: () => void;
  movie: IMovie;
}) => {
  return (
    <Modal onClose={onClose} size={"full"} isOpen={isOpen}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Confirm add to {movie.Title} your playlist?</ModalHeader>
        <ModalFooter mt="auto" backgroundColor="orange">
          <Text mr="15px">My favorite color is orange</Text>
          <Button onClick={addToPlaylist} primary colorScheme="blue" mr={3}>
            Confirm
          </Button>
          <Button onClick={onClose}>Cancel</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
export default MyModal;

import React, { useEffect, useState } from "react";
import MovieItem from "./MovieItem";
import type { IMovie } from "./hooks/useMovies";
import { Container, Link as LinkUI } from "@chakra-ui/react";
import { MyGrid } from "./App";
import { Link } from "react-router-dom";

const Playlist: React.FC = () => {
  const [movies, setMovies] = useState<IMovie[]>([]);
  useEffect(() => {
    const movies = JSON.parse(localStorage.getItem("playlist") || "[]");
    setMovies(movies);
  }, []);

  return (
    <>
      <Container p="5" maxW="xl">
        <LinkUI display="inline-block" mb="10px">
          <Link to="/">{"<<"} Back</Link>
        </LinkUI>

        <MyGrid>
          {movies.map((movie) => (
            <MovieItem key={movie.imdbID} movie={movie} />
          ))}
        </MyGrid>
      </Container>
    </>
  );
};

export default Playlist;

import React, { useState } from "react";
import type { IMovie } from "./hooks/useMovies";
import ConfirmationModal from "./Modal";
import { Link } from "react-router-dom";
import {
  Input,
  Container,
  Box,
  Spinner,
  Grid,
  Link as LinkUI,
  Wrap,
  useDisclosure,
} from "@chakra-ui/react";
import { useDebounce } from "use-debounce";
import MovieItem from "./MovieItem";
import useMovies from "./hooks/useMovies";

export const MyGrid: React.FC = ({ children }) => (
  <Grid
    templateColumns={{ md: "repeat(3, 1fr)", sm: "1fr" }}
    gap={{ md: 3 }}
    mb={{ sm: 2 }}
  >
    {children}
  </Grid>
);

const App: React.FC = () => {
  const [searchMovie, setSearchMovie] = useState("");
  const [debouncedSearchMovie] = useDebounce(searchMovie, 1500);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [toAddToPlaylistMovie, setToAddToPlaylistMovie] =
    useState<IMovie>(Object);

  const { movies, setMoviesCallback, isFetching, isError, error } =
    useMovies(debouncedSearchMovie);

  const addToPlaylistClick = (playlist: IMovie) => {
    setToAddToPlaylistMovie(playlist);
    onOpen();
  };

  const addToPlaylist = () => {
    const playlist: IMovie[] = JSON.parse(
      localStorage.getItem("playlist") || "[]"
    );
    if (!playlist.some((item) => item.imdbID === toAddToPlaylistMovie.imdbID)) {
      playlist.push({ ...toAddToPlaylistMovie, inPlaylist: true });
      localStorage.setItem("playlist", JSON.stringify(playlist));

      setMoviesCallback(playlist);
    }
    onClose();
  };

  return (
    <div>
      <ConfirmationModal
        isOpen={isOpen}
        onClose={onClose}
        movie={toAddToPlaylistMovie}
        addToPlaylist={addToPlaylist}
      />
      {isFetching && (
        <Spinner
          pos="absolute"
          top={"100%"}
          left={"50%"}
          transform="transform: translate(-50%, -50%)"
        />
      )}

      <Container p="5" maxW="xl">
        <Wrap mb="10px" display="flex" justifyContent="right">
          <LinkUI>
            <Link to="/playlist">My playlist {">>"}</Link>
          </LinkUI>
        </Wrap>
        <Input
          placeholder="Search any movie"
          onChange={(e) => {
            setSearchMovie(e.target.value);
          }}
        />

        <Box mt="4">
          {isError && typeof error === "object" && <p>{error?.toString()}</p>}
        </Box>

        <MyGrid>
          {!isError &&
            debouncedSearchMovie.trim().length >= 3 &&
            !isFetching &&
            movies?.map((movie) => (
              <MovieItem
                key={movie.imdbID} 
                movie={movie}
                addToPlaylistClick={addToPlaylistClick}
              />
            ))}
        </MyGrid>
      </Container>
    </div>
  );
};

export default App;

import { useQuery } from "react-query";
import { useState } from "react";

export interface IMovie {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
  inPlaylist?: boolean;
}

interface IMoviesResponse {
  Search: IMovie[];
  totalResults: string;
  Response: string;
}

const useMovies = (searchQuery: string) => {
  const [movies, setMovies] = useState<IMovie[]>([]);

  const { isFetching, isError, error } = useQuery<IMoviesResponse>(
    ["movies", searchQuery],
    async () => {
      const response = await fetch(
        `https://cors-anywhere.herokuapp.com/http://www.omdbapi.com/?apikey=d0b5dc2&s=${searchQuery}`
      );
      const result = await response.json();
      if (result.Response === "False") throw new Error(result.Error);

      return result;
    },
    {
      enabled: searchQuery.length >= 3,
      onSuccess: (data) => {
        if (data?.Search.length) {
          const moviesFromPlaylist = JSON.parse(
            localStorage.getItem("playlist") || "[]"
          );

          const nextMovies = mergeMovieInPlaylistWithMovies(
            data.Search,
            moviesFromPlaylist
          ).filter(Boolean);

          setMovies(nextMovies);
        }
      },
    }
  );

  const setMoviesCallback = (newSavedPlaylist: IMovie[]) => {
    const nextMovies = mergeMovieInPlaylistWithMovies(movies, newSavedPlaylist);
    setMovies(nextMovies);
  };

  return {
    movies,
    setMoviesCallback,
    isFetching,
    isError,
    error,
  };
};

const mergeMovieInPlaylistWithMovies = (
  moviesFromApi: IMovie[],
  moviesFromPlaylist: IMovie[]
) => {
  return moviesFromApi.map((movie) => {
    const foundMovie = moviesFromPlaylist.find(
      (movie2) => movie.imdbID === movie2.imdbID
    );
    if (foundMovie) {
      return {
        ...foundMovie,
      };
    }
    return movie;
  });
};

export default useMovies;
